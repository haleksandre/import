import { resolve } from "path";

(async () => {
    const files = [
        resolve(__dirname, "file01"),
        resolve(__dirname, "file02"),
        resolve(__dirname, "file03"),
    ];

    for (const file of files) {
        try {
            const { test } = await import(file);

            console.log(test);
        } catch (error) {
            console.log(error);
        }
    }
})();
